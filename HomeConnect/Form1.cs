﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeConnect
{
    public partial class frmMain : Form
    {
        VseeApi api = new VseeApi("ddzmr7oiizoyyjszdzvtslkvbzqyxxdirrs6sbk9yhogzoqpzkkecw1ku5sg9vwg",
                                  "zdt0wpw3tmhkw0aogolccyruy6puttao8hsr14j3m2x6oi3shusw6cjtf0fz8gjn");

        private int SC_MONITORPOWER = 0xF170;

        private uint WM_SYSCOMMAND = 0x0112;

        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
        
        enum MonitorState
        {
            ON = -1,
            OFF = 2,
            STANDBY = 1
        }
        
        private void SetMonitorState(MonitorState state)
        {
            SendMessage(this.Handle, WM_SYSCOMMAND, (IntPtr)SC_MONITORPOWER, (IntPtr)state);

        }

        //  SetMonitorState(MonitorState.ON);



        public frmMain()
        {
            InitializeComponent();
        }
        
        private void frmMain_Load(object sender, EventArgs e)
        {
            frmMain_ResizeEnd(sender, e);
        }

        private void frmMain_ResizeEnd(object sender, EventArgs e)
        {
            pnlContacts.Top = 50; pnlContacts.Left = 50;
            pnlContacts.Width = this.Width - 100; pnlContacts.Height = this.Height - 100;

            pnlCall.Top = 50; pnlCall.Left = this.Width + 50;
            pnlCall.Width = this.Width - 100; pnlCall.Height = this.Height - 100;

            

        }

        private void pnlCall_Resize(object sender, EventArgs e)
        {

            picSpinner.Top = (pnlCall.Height - 240) / 2;
            picSpinner.Left = (pnlCall.Width - 240) / 2;
           
        }






        // Animation Things

        // Is destination window call or contacts
        bool animation_dest_call = false;
        bool animation_curr_call = false;

        int animation_max_vel = 100;

        private void timerAnimate_Tick(object sender, EventArgs e)
        {
            if(!animation_curr_call && animation_dest_call)
            {
                // Show call
                if(pnlContacts.Left > 0 - pnlContacts.Width)
                {
                    pnlContacts.Left -= Math.Min( ((pnlContacts.Left-(0- pnlContacts.Width)) / 4) +1, animation_max_vel);
                    pnlCall.Left = pnlContacts.Left + pnlContacts.Width + 50;

                } else
                {
                    pnlContacts.Left = 0 - this.Width;
                    animation_curr_call = true;
                    pnlContacts.Visible = false;
                    timerAnimate.Enabled = false;
                }

            } else if(animation_curr_call && !animation_dest_call)
            {
                // Hide call
                if(pnlContacts.Left < 50)
                {
                    pnlContacts.Left += Math.Min( ((50-pnlContacts.Left) / 4) +1 , animation_max_vel);
                    pnlCall.Left = pnlContacts.Left + pnlContacts.Width + 50;

                } else
                {
                    pnlContacts.Left = 50;
                    animation_curr_call = false;
                    pnlCall.Visible = false;
                    timerAnimate.Enabled = false;
                }

            }
        }

        void animation_show_call()
        {
            pnlContacts.Visible = true;
            pnlCall.Visible = true;

            animation_dest_call = true;
            timerAnimate.Enabled = true;
        }

        void animation_hide_call()
        {
            pnlContacts.Visible = true;
            pnlCall.Visible = true;

            animation_dest_call = false;
            timerAnimate.Enabled = true;
        }

        private void pnlContacts_Click(object sender, EventArgs e)
        {
        }

        private void pnlCall_Click(object sender, EventArgs e)
        {
        }



        // Contacts resize

        int contacts_border = 50;

        private void pnlContacts_Resize(object sender, EventArgs e)
        {
            picContact1.Top = contacts_border;
            picContact1.Left = contacts_border;
            picContact1.Height = (pnlContacts.Height - contacts_border*3) / 2;
            picContact1.Width = (pnlContacts.Width - contacts_border*3) / 2;

            picContact2.Top = picContact1.Top;
            picContact2.Left = picContact1.Left + picContact2.Width + contacts_border;
            picContact2.Height = picContact1.Height;
            picContact2.Width = picContact1.Width;

            picContact3.Left = picContact1.Left;
            picContact3.Top = picContact1.Top + picContact1.Height + contacts_border;
            picContact3.Height = picContact1.Height;
            picContact3.Width = picContact1.Width;

            picContact4.Top = picContact2.Top + picContact2.Height + contacts_border;
            picContact4.Left = picContact3.Left + picContact3.Width + contacts_border;
            picContact4.Height = picContact1.Height;
            picContact4.Width = picContact1.Width;


        }

        private void picContact1_Click(object sender, EventArgs e)
        {
            orchestrator_action = "call";
            orchestrator_param = "james@vsee.com";
            timerOrchestrator.Enabled = true;
        }

        private void picContact2_Click(object sender, EventArgs e)
        {
            orchestrator_action = "call";
            orchestrator_param = "demo@vsee.com";
            timerOrchestrator.Enabled = true;
        }

        private void picContact3_Click(object sender, EventArgs e)
        {
            orchestrator_action = "call";
            orchestrator_param = "demo@vsee.com";
            timerOrchestrator.Enabled = true;
        }

        private void picContact4_Click(object sender, EventArgs e)
        {
            orchestrator_action = "call";
            orchestrator_param = "demo@vsee.com";
            timerOrchestrator.Enabled = true;
        }



        // Orchestrator

        string orchestrator_action = "";
        string orchestrator_param = "";
        string orchestrator_uri = "";

        private void timerOrchestrator_Tick(object sender, EventArgs e)
        {
            switch(orchestrator_action)
            {
                case "call":

                    if (!animation_curr_call)
                    {
                        orchestrator_uri = "";
                        animation_show_call();
                        this.TopMost = true;
                    }
                    else if (timerAnimate.Enabled) return;  // wait for animation to complete
                    else if (orchestrator_uri == "")
                    {
                        // Kill vsee, if any
                        killVsee();
                        // generate uri
                        orchestrator_uri = makeCallUri(orchestrator_param);
                        // Trigger uri
                        startVsee(orchestrator_uri);

                    }
                    else if (!isVseeRunning()) return;  // check if process exists
                    else
                    {
                        // poll for termination
                        orchestrator_action = "terminate";
                    }

                    break;
                    
                case "terminate":
                    // check if process exists
                    if (isVseeRunning()) return;
                    else if (!timerAnimate.Enabled)
                    {
                        this.TopMost = false;

                        animation_hide_call();
                        // generate uri
                    }
                    else if (timerAnimate.Enabled) return;  // wait for animation to complete
                    else
                    {
                        orchestrator_action = "";
                        timerOrchestrator.Enabled = false;
                    }

                    break;

                default:
                    orchestrator_action = "";
                    timerOrchestrator.Enabled = false;
                    break;
            }
        }



        // api things

        

        string makeCallUri(string target)
        {
            VseeApi.CommandSet call_command = new VseeApi.CommandSet();
            //Point loc1 = pnlCall.PointToScreen(pnlCall.Location);
            //Point loc2 = pnlCall.PointToScreen(pnlCall.Location + pnlCall.Size);
            Point loc1 = this.Location;
            Point loc2 = loc1 + this.Size;

            call_command.AddJson(
                @"[
                    {
                        ""init"": {
                            ""commands"": [
                                {
                                    ""setUser"": { ""username"": ""vsee+jamestest123"" }
                                },
                                {
                                    ""setAddressBook"": { ""enabled"": false }
                                },
                                {
                                    ""setFirstTutorial"": { ""enabled"": false }
                                },
                                {""setScreenShare"" : {""enabled"" : false, ""receiverAllowDrawing"" : false, ""senderAllowTakeControl"" : false, ""receiverAllowTakeControl"" : false}},
                                {""setAutoLaunch"" : {""enabled"" : false}},

                                {""setLocalVideo"" : {""enabled"" : true, ""resolution"":""high""}},

                                {""setMergeVideoWindows"" : {""enabled"":true, ""height"":" + (loc2.Y - loc1.Y) + @",
                                    ""pinned"" : true, ""width"" : " + (loc2.X-loc1.X) + @", ""force"" : true, ""y"" : " + loc1.Y + @",
                                    ""x"" : " + loc1.X + @"}}
                            ]
                        }
                    },
                    {
                        ""onEndCall"": {
                            ""commands"": [
                                {
                                   ""exit"" : {""restorecreds"" : false}
                                }
                            ]
                        }
                    },
                    {
                        ""call"": {
                            ""username"": """ + target + @"""
                        }
                    }

                ]");

            /*
            {""setVideoWindow"" : {""username"" : """ + target + @""", ""height"" : " + pnlCall.Height + @",
                ""pinned"" : true, ""width"" : " + pnlCall.Width + @", ""y"" : " + pnlCall.Top + @", ""x"" : " + pnlCall.Left + @"}}

            */

            string uri = api.CommandUri(call_command);
            return uri;
        }

        bool isVseeRunning()
        {
            Process[] processes = Process.GetProcessesByName("vsee");
            if (processes.Length > 0)
                return true;
            return false;
        }

        void killVsee()
        {
            Process[] processes = Process.GetProcessesByName("vsee");
            if (processes.Length > 0)
                processes[0].Kill();
        }

        void startVsee(string uri)
        {
            string appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);


            ProcessStartInfo startInfo = new ProcessStartInfo(appData + "\\VSeeInstall\\vsee.exe", " -uri " + orchestrator_uri);
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo);
        }
        
    }
}
