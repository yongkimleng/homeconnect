﻿namespace HomeConnect
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pnlContacts = new System.Windows.Forms.Panel();
            this.picContact4 = new System.Windows.Forms.PictureBox();
            this.picContact3 = new System.Windows.Forms.PictureBox();
            this.picContact2 = new System.Windows.Forms.PictureBox();
            this.picContact1 = new System.Windows.Forms.PictureBox();
            this.pnlCall = new System.Windows.Forms.Panel();
            this.picSpinner = new System.Windows.Forms.PictureBox();
            this.timerAnimate = new System.Windows.Forms.Timer(this.components);
            this.timerOrchestrator = new System.Windows.Forms.Timer(this.components);
            this.pnlContacts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picContact4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picContact3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picContact2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picContact1)).BeginInit();
            this.pnlCall.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSpinner)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlContacts
            // 
            this.pnlContacts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(192)))), ((int)(((byte)(186)))));
            this.pnlContacts.Controls.Add(this.picContact4);
            this.pnlContacts.Controls.Add(this.picContact3);
            this.pnlContacts.Controls.Add(this.picContact2);
            this.pnlContacts.Controls.Add(this.picContact1);
            this.pnlContacts.Location = new System.Drawing.Point(12, 12);
            this.pnlContacts.Name = "pnlContacts";
            this.pnlContacts.Size = new System.Drawing.Size(596, 326);
            this.pnlContacts.TabIndex = 0;
            this.pnlContacts.Click += new System.EventHandler(this.pnlContacts_Click);
            this.pnlContacts.Resize += new System.EventHandler(this.pnlContacts_Resize);
            // 
            // picContact4
            // 
            this.picContact4.BackColor = System.Drawing.Color.White;
            this.picContact4.Image = ((System.Drawing.Image)(resources.GetObject("picContact4.Image")));
            this.picContact4.Location = new System.Drawing.Point(201, 180);
            this.picContact4.Name = "picContact4";
            this.picContact4.Size = new System.Drawing.Size(519, 133);
            this.picContact4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picContact4.TabIndex = 3;
            this.picContact4.TabStop = false;
            this.picContact4.Click += new System.EventHandler(this.picContact4_Click);
            // 
            // picContact3
            // 
            this.picContact3.BackColor = System.Drawing.Color.White;
            this.picContact3.Image = ((System.Drawing.Image)(resources.GetObject("picContact3.Image")));
            this.picContact3.Location = new System.Drawing.Point(24, 180);
            this.picContact3.Name = "picContact3";
            this.picContact3.Size = new System.Drawing.Size(246, 72);
            this.picContact3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picContact3.TabIndex = 2;
            this.picContact3.TabStop = false;
            this.picContact3.Click += new System.EventHandler(this.picContact3_Click);
            // 
            // picContact2
            // 
            this.picContact2.BackColor = System.Drawing.Color.White;
            this.picContact2.Image = ((System.Drawing.Image)(resources.GetObject("picContact2.Image")));
            this.picContact2.Location = new System.Drawing.Point(314, 36);
            this.picContact2.Name = "picContact2";
            this.picContact2.Size = new System.Drawing.Size(246, 72);
            this.picContact2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picContact2.TabIndex = 1;
            this.picContact2.TabStop = false;
            this.picContact2.Click += new System.EventHandler(this.picContact2_Click);
            // 
            // picContact1
            // 
            this.picContact1.BackColor = System.Drawing.Color.White;
            this.picContact1.Image = ((System.Drawing.Image)(resources.GetObject("picContact1.Image")));
            this.picContact1.Location = new System.Drawing.Point(24, 36);
            this.picContact1.Name = "picContact1";
            this.picContact1.Size = new System.Drawing.Size(246, 72);
            this.picContact1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picContact1.TabIndex = 0;
            this.picContact1.TabStop = false;
            this.picContact1.Click += new System.EventHandler(this.picContact1_Click);
            // 
            // pnlCall
            // 
            this.pnlCall.BackColor = System.Drawing.Color.White;
            this.pnlCall.Controls.Add(this.picSpinner);
            this.pnlCall.Location = new System.Drawing.Point(326, 364);
            this.pnlCall.Name = "pnlCall";
            this.pnlCall.Size = new System.Drawing.Size(668, 345);
            this.pnlCall.TabIndex = 2;
            this.pnlCall.Visible = false;
            this.pnlCall.Click += new System.EventHandler(this.pnlCall_Click);
            this.pnlCall.Resize += new System.EventHandler(this.pnlCall_Resize);
            // 
            // picSpinner
            // 
            this.picSpinner.BackColor = System.Drawing.Color.Transparent;
            this.picSpinner.Image = ((System.Drawing.Image)(resources.GetObject("picSpinner.Image")));
            this.picSpinner.Location = new System.Drawing.Point(233, 41);
            this.picSpinner.Name = "picSpinner";
            this.picSpinner.Size = new System.Drawing.Size(240, 240);
            this.picSpinner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picSpinner.TabIndex = 0;
            this.picSpinner.TabStop = false;
            // 
            // timerAnimate
            // 
            this.timerAnimate.Interval = 17;
            this.timerAnimate.Tick += new System.EventHandler(this.timerAnimate_Tick);
            // 
            // timerOrchestrator
            // 
            this.timerOrchestrator.Tick += new System.EventHandler(this.timerOrchestrator_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(104)))), ((int)(((byte)(130)))));
            this.ClientSize = new System.Drawing.Size(1006, 721);
            this.Controls.Add(this.pnlCall);
            this.Controls.Add(this.pnlContacts);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HomeConnect";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResizeEnd += new System.EventHandler(this.frmMain_ResizeEnd);
            this.pnlContacts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picContact4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picContact3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picContact2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picContact1)).EndInit();
            this.pnlCall.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picSpinner)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlContacts;
        private System.Windows.Forms.Panel pnlCall;
        private System.Windows.Forms.PictureBox picSpinner;
        public System.Windows.Forms.Timer timerAnimate;
        private System.Windows.Forms.PictureBox picContact2;
        private System.Windows.Forms.PictureBox picContact1;
        private System.Windows.Forms.PictureBox picContact4;
        private System.Windows.Forms.PictureBox picContact3;
        public System.Windows.Forms.Timer timerOrchestrator;
    }
}

