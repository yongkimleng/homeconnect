﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Threading.Tasks;

namespace HomeConnect
{
    class VseeApi
    {
        private string apikey;
        private string apisecret;
        const string url = @"https://api.vsee.com/";

        public VseeApi(string key, string secret)
        {
            apikey = key; apisecret = secret;
        }

        //
        // Create URI from commandset
        // 
        public string CommandUri(CommandSet commandset)
        {
            List<CommandSet> c = new List<CommandSet>();
            c.Add(commandset);
            List<string> uris = CommandUri(c);
            if (uris.Count < 1) return null;
            return uris[0];
        }

        //
        // Creates multiple URIs from multiple commandsets
        //
        public List<string> CommandUri(List<CommandSet> commands)
        {
            Dictionary<string, dynamic> p = new Dictionary<string, dynamic>();
            p.Add("uris", commands);


            List<string> retstrs = new List<string>();

            Dictionary<string, dynamic> ret = VseePost("uri/create", p);
            if(ret.ContainsKey("data"))
            {
                for(int i = 0; i < commands.Count; ++i)
                {
                    retstrs.Add(ret["data"][i]);
                }
            }

            return retstrs;
        }






        //
        //
        // Helpers
        //
        //

        public class CommandSet : List<Dictionary<string, dynamic>>
        {
            public void AddJson(string json)
            {
                this.AddRange(
                    new JavaScriptSerializer().Deserialize<List<Dictionary<string, dynamic>>>(json));
            }
        }

        //
        // VSee API HTTP Post
        //
        private Dictionary<string, dynamic> VseePost(string suburl, Dictionary<string, dynamic> parameters)
        {
            // append API key and API secret
            suburl = url + suburl + "?apikey=" + apikey;
            parameters.Add("secretkey", apisecret);
            return HttpPost(suburl, parameters);
        }

        //
        // Standard HTTP Post
        //
        private Dictionary<string, dynamic> HttpPost(string customurl, Dictionary<string, dynamic> parameters)
        {
            string payload = "";

            if (parameters != null)
            {
                payload = new JavaScriptSerializer().Serialize(parameters);
            }
           
            HttpWebRequest hwr = (HttpWebRequest)WebRequest.Create(customurl);
            hwr.ContentType = "text/json";
            hwr.Method = "POST";
            using (StreamWriter str = new StreamWriter(hwr.GetRequestStream()))
            {
                str.Write(payload);
            }

            using (HttpWebResponse res = (HttpWebResponse)hwr.GetResponse())
            {
                using (StreamReader sr = new StreamReader(res.GetResponseStream()))
                {
                    // Expect encrypted json params
                    string restext = sr.ReadToEnd();
                    if ((int)(res.StatusCode) != 200)
                    {
                        throw new Exception("HttpPost != 200: " + restext);
                    }
                    if (restext != "")
                    {
                        return new JavaScriptSerializer().Deserialize<Dictionary<string, dynamic>>(restext);
                    }
                    return null;
                }
            }
        }
    }
}
